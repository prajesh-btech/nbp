
Write a java program which retrieves and displays currency exchange rate published by NBP (Polish
National Bank).

Data retrieved from NBP are “buy” (kurs_kupna) and “sale” (kurs_sprzedazy) rates.
The program should accept the following parameters in the standard input:
- currency code (USD, EUR, CHF, GBP)
- Time frame – start date and end date
The program should return the following data on the standard output
- Average “buy” rate of given currency in the specified time period
- The standard deviation of “sale” rate for the same currency and time period
Additional requirements:
- Data should be retrieved as described below
- Results should be displayed on standard output
- All files should be located in the package named pl.parser.nbp
- The method “main” should be written in the class “MainClass”

Tips:
- You can write as many classes as you need
- To retrieve data from NBP please follow the instruction:
http://www.nbp.pl/home.aspx?f=/kursy/instrukcja_pobierania_kursow_walut.html
(for non-polish speakers see the description at the end of the task)
- The sample file with rates : http://www.nbp.pl/kursy/xml/c073z070413.xml
- data_publikacji – date of issue
- kod_waluty – currency code
- kurs_kupna – buy rate
- kurs_sprzedazy – sell rate
- you can choose any technology/tool to parse XML file
- date of issue (data_publikacji) should be a reference date
- files issued in the dates provided as a parameters must be taken into account
- if you want to use external libraries we expect maven project with all dependencies added
to pom.xml file

The following command should run the program:

java pl.parser.nbp.MainClass EUR 2013-01-28 2013-01-31

The proper output for the parameters used in the sample:

4,1505
0,0125

Description of parameters and output:
EUR – currency code
2013-01-28 – start date
2013-01-31 - end date
4.1505 – the average “buy” rate
0,0125 the standard deviation of “sell” rate

Retrieving data from NBP

The file in XML format with NBP rates are published at www.nbp.pl in the folder:
/kursy/xml/*.xml

Additionally there is a file:
http://www.nbp.pl/kursy/xml/dir.txt

which contains the list of names of the files with rates (without .xml extension)
Since 1st July 2015 the file dir.txt contains only data from current year. Data from previous years can
be found in files dir2002.txt, dir2003.txt …. dir2015.txt

The names of all files have the same format:
'xnnnzrrmmdd.xml'
Where
x – the letter which determines the type of file:
a – the table with average rate of foreign currencies
b – the table with average rate of non-exchangable currencies
c – the table with buy and sell rates
h – the table with exchange units
nnn – reference number of the table
‘z’ – constant element
rrmmdd – date of issue

The sample how to get xml with average rates issued on 5th of February 2010r:
In the file dir2010.txt look for the desired date:

a024z100204
c025z100205
h025z100205
a025z100205
c026z100208
h026z100208
So the path to the file is http://www.nbp.pl/kursy/xml/a025z100205.xml