package pl.parser.nbp;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.validator.GenericValidator;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by RAJESH on 11/10/2019.
 */
public class Application {

    private static final Logger LOGGER = Logger.getLogger(Application.class.getName());

    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String HOME_CURRENCY = "PLN";

    public List<URL> init(String currency, String startDate, String endDate) throws ParseException, IOException {

        //Currency validation
        if (!CurrencyValidator.isValid(currency)) {
            throw new InvalidInputException("Invalid currency :" + currency);
        }
        if (currency.equals(HOME_CURRENCY)) {
            throw new InvalidInputException("Home currency : [" + currency + "] can't be input");
        }

        // Date validation
        if (!GenericValidator.isDate(startDate, DATE_FORMAT, true)
                || !GenericValidator.isDate(endDate, DATE_FORMAT, true)) {
            throw new InvalidInputException("Invalid start or end date :[" + startDate + " - " + endDate + "]");
        }

        // Date range validation
        Date parsedStartDate = DateUtil.parse(startDate, DATE_FORMAT);
        Date parsedEndDate = DateUtil.parse(endDate, DATE_FORMAT);

        if (parsedStartDate.after(parsedEndDate)) {
            throw new InvalidDateRangeException("Invalid date range :[" + startDate + " - " + endDate + "]");
        }

        LOGGER.log(Level.INFO, "start date :" + startDate);
        LOGGER.log(Level.INFO, "end date :" + endDate);

        List<URL> dirURLs = NBPFilePathUtil.getDirFileURLs(parsedStartDate, parsedEndDate);

        List<URL> xmlFileURLs = null;
        if (CollectionUtils.isNotEmpty(dirURLs)) {
            xmlFileURLs = NBPFilePathUtil.getXMLFileURLs(parsedStartDate, parsedEndDate, dirURLs);
        }

        return xmlFileURLs;

    }

}
