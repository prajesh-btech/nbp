package pl.parser.nbp;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;

import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by RAJESH on 11/10/2019.
 */
public class MainClass {

    private static final Logger LOGGER = Logger.getLogger(MainClass.class.getName());

    private static final int MAX_FRACTIONAL_LIMIT = 4;

    public static void main(String[] args) {

        int length = args.length;
        if (length > 3 || length < 3) {
            LOGGER.log(Level.WARNING, " args length must be 3");
            System.exit(1);
        }

        String currency = args[0];
        String startDate = args[1];
        String endDate = args[2];

        Application application = new Application();

        try {
            //Retrieve the matched xml file URLs
            List<URL> xmlFileURLs = application.init(currency, startDate, endDate);

            //Parse the xml files and retrieve all the currency matched Positions
            List<Position> allPositions = getPositions(currency, xmlFileURLs);

            // collect buyRates, sellRates from all the currency matched Positions
            if (CollectionUtils.isNotEmpty(allPositions)) {

                Position[] positions = new Position[allPositions.size()];
                positions = allPositions.toArray(positions);

                double[] buyRates = new double[positions.length];
                double[] sellRates = new double[positions.length];

                // NumberFormat to parse the buyRate, SellRate String and format it to Polish standard
                NumberFormat numberFormat = NumberFormat.getInstance(new Locale("pl", "PL"));
                numberFormat.setMaximumFractionDigits(MAX_FRACTIONAL_LIMIT);

                for (int i = 0; i < positions.length; i++) {
                    buyRates[i] = numberFormat.parse(positions[i].getBuyRate()).doubleValue();
                    sellRates[i] = numberFormat.parse(positions[i].getSellRate()).doubleValue();
                }

                double average = new Mean().evaluate(buyRates);
                double standardDeviation = new StandardDeviation(false).evaluate(sellRates);

                System.out.println("The average buy rate: " + numberFormat.format(average));
                System.out.println("The standard deviation of sell rate: " + numberFormat.format(standardDeviation));

            }

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static List<Position> getPositions(String currency, List<URL> xmlFileURLs) {
        List<Position> allPositions = null;
        if (CollectionUtils.isNotEmpty(xmlFileURLs)) {
            allPositions = new ArrayList<>();

            URLParser urlParser = URLParserFactory.getInstance();

            for (URL url : xmlFileURLs) {
                List<Position> positions = urlParser.parse(url, currency);
                allPositions.addAll(positions);
            }
        }
        return allPositions;
    }
}
