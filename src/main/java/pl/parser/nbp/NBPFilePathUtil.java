package pl.parser.nbp;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by RAJESH on 11/9/2019.
 */
public class NBPFilePathUtil {

    private static final Logger LOGGER = Logger.getLogger(NBPFilePathUtil.class.getName());

    private static final int DATA_AVAILABLE_SINCE_YEAR = 2002;
    private static final String DIR_FILE_NAME_PREFIX = "dir";
    private static final String BASE_URL = "http://www.nbp.pl/kursy/xml/";

    private static final String XML_FILE_EXTENSION = ".xml";
    private static final String TXT_FILE_EXTENSION = ".txt";
    private static final String BUY_SELL_RATE_FILE_NAME_PREFIX = "c";
    private static final int DATE_START_INDEX = 5;
    private static final String INT_DATE_FORMAT = "yyMMdd";

    public static List<URL> getDirFileURLs(Date startDate, Date endDate) throws MalformedURLException {

        int startYear = DateUtil.getYear(startDate);
        int endYear = DateUtil.getYear(endDate);

        int currentYear = DateUtil.getYear(new Date());
        LOGGER.log(Level.INFO, "current year :" + currentYear);

        if (endYear < startYear
                || startYear > currentYear
                || endYear < DATA_AVAILABLE_SINCE_YEAR) {
            return null;
        }

        if (startYear < DATA_AVAILABLE_SINCE_YEAR) {
            startYear = DATA_AVAILABLE_SINCE_YEAR;
        }

        if (endYear > currentYear) {
            endYear = currentYear;
        }

        List<URL> urls = new ArrayList<>();

        for (int i = startYear; i <= endYear; i++) {
            if (i == currentYear) {
                urls.add(new URL(BASE_URL + DIR_FILE_NAME_PREFIX + TXT_FILE_EXTENSION));
            } else {
                urls.add(new URL(BASE_URL + DIR_FILE_NAME_PREFIX + String.valueOf(i) + TXT_FILE_EXTENSION));
            }
        }

        return urls;
    }

    public static List<URL> getXMLFileURLs(Date startDate, Date endDate, List<URL> urls) throws IOException {

        int startDateIntValue = DateUtil.intValue(startDate, INT_DATE_FORMAT);
        int endDateIntValue = DateUtil.intValue(endDate, INT_DATE_FORMAT);

        List<URL> list = null;
        if (CollectionUtils.isNotEmpty(urls)) {
            list = new ArrayList<>();
            for (URL url : urls) {

                try (BufferedReader in = new BufferedReader(
                        new InputStreamReader(url.openStream()))) {

                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {

                        if (inputLine.startsWith(BUY_SELL_RATE_FILE_NAME_PREFIX)) {
                            String date = inputLine.substring(DATE_START_INDEX, inputLine.length());

                            int dateInt = Integer.parseInt(date);
                            if (dateInt >= startDateIntValue && dateInt <= endDateIntValue) {

                                list.add(new URL(BASE_URL + StringUtils.trim(inputLine) + XML_FILE_EXTENSION));
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        return list;
    }
}
