package pl.parser.nbp;

import java.net.URL;
import java.util.List;

/**
 * Created by RAJESH on 11/10/2019.
 */
public interface URLParser {

    List<Position> parse(URL url, String currency);

}
