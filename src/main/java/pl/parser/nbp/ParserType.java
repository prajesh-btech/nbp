package pl.parser.nbp;

/**
 * Created by RAJESH on 11/10/2019.
 */
public enum ParserType {
    STAX("StAX");

    private final String type;

    ParserType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
