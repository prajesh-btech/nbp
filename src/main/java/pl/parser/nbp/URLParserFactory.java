package pl.parser.nbp;

import static pl.parser.nbp.ParserType.STAX;

/**
 * Created by RAJESH on 11/10/2019.
 */
public class URLParserFactory {

    public static URLParser getInstance() {
        return getInstance(STAX);
    }

    public static URLParser getInstance(ParserType parserType) {
        URLParser urlParser = null;
        switch (parserType) {
            case STAX:
                urlParser = new StAXParser();
                break;
        }

        return urlParser;
    }
}
