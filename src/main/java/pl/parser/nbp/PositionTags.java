package pl.parser.nbp;

/**
 * Created by RAJESH on 11/10/2019.
 */
public interface PositionTags {
    String POSITION = "pozycja";
    String CURRENCY_NAME = "nazwa_waluty";
    String CONVERTER = "przelicznik";
    String CURRENCY_CODE = "kod_waluty";
    String BUYING_RATE = "kurs_kupna";
    String SELLING_RATE = "kurs_sprzedazy";
}
