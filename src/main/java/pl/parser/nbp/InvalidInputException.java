package pl.parser.nbp;

/**
 * Created by RAJESH on 11/9/2019.
 */
public class InvalidInputException extends RuntimeException {

    public InvalidInputException(String message) {
        super(message);
    }

    public InvalidInputException() {
    }
}
