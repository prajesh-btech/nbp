package pl.parser.nbp;

/**
 * Created by RAJESH on 11/10/2019.
 */
public class InvalidDateRangeException extends RuntimeException {

    public InvalidDateRangeException(String message) {
        super(message);
    }

    public InvalidDateRangeException() {
    }
}
