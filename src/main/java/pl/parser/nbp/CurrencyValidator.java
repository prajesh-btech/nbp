package pl.parser.nbp;

import org.apache.commons.lang3.StringUtils;

import java.util.Currency;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by RAJESH on 11/10/2019.
 */
public class CurrencyValidator {

    private static final Logger LOGGER = Logger.getLogger(CurrencyValidator.class.getName());

    public static boolean isValid(String currencyCode) {
        if (StringUtils.isEmpty(currencyCode) || StringUtils.isBlank(currencyCode)) {
            LOGGER.log(Level.WARNING, " Currency is empty");
            return false;
        }
        try {
            Currency currency = Currency.getInstance(currencyCode);
            LOGGER.log(Level.INFO, "currency :" + currency.getCurrencyCode());
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "invalid Currency [" + currencyCode + "], The Currency code must be [EUR, USD, ...]");
            return false;
        }

        return true;
    }

}
