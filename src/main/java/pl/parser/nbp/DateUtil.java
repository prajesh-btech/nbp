package pl.parser.nbp;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by RAJESH on 11/9/2019.
 */
public class DateUtil {

    public static Date parse(String date, String pattern) throws ParseException {
        return DateUtils.parseDate(date, pattern);
    }

    public static int getYear(Date date) {
        Calendar calendar = getCalendar(date);
        return calendar.get(Calendar.YEAR);
    }

    private static Calendar getCalendar(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        return calendar;
    }

    public static int intValue(Date date, String format) {
        Calendar calendar = getCalendar(date);
        return Integer.parseInt(DateFormatUtils.format(calendar, format));
    }
}
