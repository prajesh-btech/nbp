package pl.parser.nbp;

import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAJESH on 11/10/2019.
 */
public class StAXParser implements URLParser {

    private static XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

    @Override
    public List<Position> parse(URL url, String currency) {
        if (url == null || StringUtils.isEmpty(currency)
                || StringUtils.isBlank(currency)) {
            return null;
        }
        String urlCurrency = null;
        String buyRate = null;
        String sellRate = null;
        Position position = null;
        List<Position> positions = null;

        try (InputStream xml = url.openStream()) {

            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(xml);

            while (xmlEventReader.hasNext()) {
                XMLEvent event = xmlEventReader.nextEvent();
                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();
                    switch (startElement.getName().getLocalPart()) {
                        case PositionTags.POSITION:
                            position = new Position();
                            break;
                        case PositionTags.CURRENCY_NAME:
                            String currencyName = xmlEventReader.nextEvent().asCharacters().getData();
                            position.setCurrencyName(currencyName);
                            break;
                        case PositionTags.CONVERTER:
                            int converter = Integer.parseInt(xmlEventReader.nextEvent().asCharacters().getData());
                            position.setConverter(converter);
                            break;
                        case PositionTags.CURRENCY_CODE:
                            urlCurrency = xmlEventReader.nextEvent().asCharacters().getData();
                            position.setCurrencyCode(urlCurrency);
                            break;
                        case PositionTags.BUYING_RATE:
                            buyRate = xmlEventReader.nextEvent().asCharacters().getData();
                            position.setBuyRate(buyRate);
                            break;
                        case PositionTags.SELLING_RATE:
                            sellRate = xmlEventReader.nextEvent().asCharacters().getData();
                            position.setSellRate(sellRate);
                            break;
                    }
                }

                if (event.isEndElement()
                        && event.asEndElement().getName().getLocalPart().equals(PositionTags.POSITION)
                        && urlCurrency.equals(currency)) {

                    if (positions == null) {
                        positions = new ArrayList<>();
                    }
                    positions.add(position);

                    break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return positions;
    }
}
