package pl.parser.nbp;

import org.junit.Test;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by RAJESH on 11/10/2019.
 */
public class DateUtilTest {

    @Test(expected = IllegalArgumentException.class)
    public void testNullDateAndNullPattern() throws ParseException {
        DateUtil.parse(null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullDateAndValidPattern() throws ParseException {
        DateUtil.parse(null, "yyyy-MM-dd");
    }

    @Test(expected = NullPointerException.class)
    public void testValidDateAndNullPattern() throws ParseException {
        DateUtil.parse("2019-01-29", null);
    }

    @Test
    public void testValidDateAndValidPattern() throws ParseException {
        assertNotNull(DateUtil.parse("2019-01-29", "yy-MM-dd"));
    }

    @Test(expected = NullPointerException.class)
    public void testNullDateInput() {
        DateUtil.getYear(null);
    }

    @Test
    public void testValidDateInput() {
        Calendar calendar = Calendar.getInstance();
        assertEquals(calendar.get(Calendar.YEAR), DateUtil.getYear(new Date()));
    }

    @Test
    public void testDateIntValue() throws ParseException {
        Date date = DateUtil.parse("2018-02-20", "yyyy-MM-dd");
        assertEquals(180220, DateUtil.intValue(date, "yyMMdd"));
    }


}
