package pl.parser.nbp;

import org.junit.Test;

import java.io.IOException;
import java.text.ParseException;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by RAJESH on 11/10/2019.
 */
public class CurrencyValidatorTest {

    @Test
    public void testNullCurrency() throws IOException, ParseException {
        assertFalse(CurrencyValidator.isValid(null));
    }

    @Test
    public void testEmptyCurrency() throws IOException, ParseException {
        assertFalse(CurrencyValidator.isValid(" "));
    }

    @Test
    public void testInvalidCurrency() throws IOException, ParseException {
        assertFalse(CurrencyValidator.isValid("eur"));
    }

    @Test
    public void testValidCurrency() throws IOException, ParseException {
        assertTrue(CurrencyValidator.isValid("EUR"));
    }

    @Test
    public void testInvalidNumberCurrency() throws IOException, ParseException {
        assertFalse(CurrencyValidator.isValid("123"));
    }


}
