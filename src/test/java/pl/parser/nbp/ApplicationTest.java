package pl.parser.nbp;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.List;

/**
 * Created by RAJESH on 11/10/2019.
 */
public class ApplicationTest {

    private Application application = null;

    @Before
    public void setUp() throws Exception {
        application = new Application();
    }

    @After
    public void tearDown() throws Exception {
        application = null;
    }

    @Test(expected = InvalidInputException.class)
    public void testNullCurrency() throws IOException, ParseException {
        String currency = null;
        String startDate = null;
        String endDate = null;

        application.init(currency, startDate, endDate);

    }

    @Test(expected = InvalidInputException.class)
    public void testInvalidCurrency() throws IOException, ParseException {
        String currency = "eur";
        String startDate = null;
        String endDate = null;

        application.init(currency, startDate, endDate);

    }

    @Test(expected = InvalidInputException.class)
    public void testValidButHomeCurrency() throws IOException, ParseException {
        String currency = "PLN";
        String startDate = null;
        String endDate = null;

        application.init(currency, startDate, endDate);

    }

    @Test(expected = InvalidInputException.class)
    public void testValidCurrencyButStartAndEndDateNull() throws IOException, ParseException {
        String currency = "EUR";
        String startDate = null;
        String endDate = null;

        application.init(currency, startDate, endDate);

    }

    @Test(expected = InvalidInputException.class)
    public void testValidCurrencyButStartDateNull() throws IOException, ParseException {
        String currency = "EUR";
        String startDate = null;
        String endDate = "2019-10-21";

        application.init(currency, startDate, endDate);

    }

    @Test(expected = InvalidInputException.class)
    public void testValidCurrencyButEndDateNull() throws IOException, ParseException {
        String currency = "EUR";
        String startDate = "2019-10-21";
        String endDate = null;

        application.init(currency, startDate, endDate);

    }

    @Test(expected = InvalidInputException.class)
    public void testValidCurrencyButInvalidStartDateFormat() throws IOException, ParseException {
        String currency = "EUR";
        String startDate = "19-10-21";
        String endDate = null;

        application.init(currency, startDate, endDate);

    }

    @Test(expected = InvalidInputException.class)
    public void testValidCurrencyAndValidStartDateButInvalidEndDateFormat() throws IOException, ParseException {
        String currency = "EUR";
        String startDate = "2019-01-28";
        String endDate = "19-10-21";

        application.init(currency, startDate, endDate);

    }

    @Test(expected = InvalidInputException.class)
    public void testValidCurrencyAndValidStartDateButInvalidLeapYear() throws IOException, ParseException {
        String currency = "EUR";
        String startDate = "2019-01-28";
        String endDate = "2019-02-29";

        application.init(currency, startDate, endDate);

    }

    @Test(expected = InvalidDateRangeException.class)
    public void testValidCurrencyAndValidStartDateAndEndDateButStartDateAfterEndDate()
            throws IOException, ParseException {
        String currency = "EUR";
        String startDate = "2019-01-20";
        String endDate = "2019-01-19";

        application.init(currency, startDate, endDate);

    }

    @Test
    public void testValidCurrencyAndValidStartDateAndEndDateButNoDataYear()
            throws IOException, ParseException {
        String currency = "EUR";
        String startDate = "2000-01-20";
        String endDate = "2001-01-19";

        List<URL> urlList = application.init(currency, startDate, endDate);
        Assert.assertNull(urlList);

    }

    @Test
    public void testValidCurrencyAndValidStartDateAndEndDateWithDataYear()
            throws IOException, ParseException {
        String currency = "EUR";
        String startDate = "2013-01-28";
        String endDate = "2013-01-31";

        List<URL> urlList = application.init(currency, startDate, endDate);
        Assert.assertNotNull(urlList);
        Assert.assertEquals(4, urlList.size());

    }


}
