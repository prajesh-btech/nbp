package pl.parser.nbp;

import org.junit.Assert;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Created by RAJESH on 11/10/2019.
 */
public class NBPFilePathUtilTest {

    @Test
    public void testNoDataWithStartAndEndDate() throws MalformedURLException, ParseException {
        Date start = DateUtil.parse("1999-01-10", "yyyy-MM-dd");
        Date end = DateUtil.parse("2000-02-10", "yyyy-MM-dd");
        List<URL> urlList = NBPFilePathUtil.getDirFileURLs(start, end);
        Assert.assertNull(urlList);
    }

    @Test
    public void testNoDataWithStartDateAfterEndDate() throws MalformedURLException, ParseException {
        Date start = DateUtil.parse("2018-01-10", "yyyy-MM-dd");
        Date end = DateUtil.parse("2015-02-10", "yyyy-MM-dd");
        List<URL> urlList = NBPFilePathUtil.getDirFileURLs(start, end);
        Assert.assertNull(urlList);
    }

    @Test
    public void testWithNoDataStartDateAndDataAvailableEndDate() throws MalformedURLException, ParseException {
        Date start = DateUtil.parse("1999-01-10", "yyyy-MM-dd");
        Date end = DateUtil.parse("2002-02-10", "yyyy-MM-dd");
        List<URL> urlList = NBPFilePathUtil.getDirFileURLs(start, end);
        Assert.assertNotNull(urlList);
        Assert.assertEquals(1, urlList.size());
    }

    @Test
    public void testWithDataAvailableStartAndEndDate() throws MalformedURLException, ParseException {
        Date start = DateUtil.parse("2002-01-10", "yyyy-MM-dd");
        Date end = DateUtil.parse("2005-02-10", "yyyy-MM-dd");
        List<URL> urlList = NBPFilePathUtil.getDirFileURLs(start, end);
        Assert.assertNotNull(urlList);
        Assert.assertEquals(4, urlList.size());
    }
}
