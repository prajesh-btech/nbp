package pl.parser.nbp;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by RAJESH on 11/10/2019.
 */
public class StAXParserTest {

    private URLParser urlParser = null;

    @Before
    public void setUp() throws Exception {
        urlParser = new StAXParser();
    }

    @After
    public void tearDown() throws Exception {
        urlParser = null;
    }

    @Test
    public void testNullURLAndNullCurrency() throws MalformedURLException {
        URL url = null;
        String currency = null;
        Assert.assertNull(urlParser.parse(url, currency));
    }

    @Test(expected = MalformedURLException.class)
    public void testInvalidURLAndNullCurrency() throws MalformedURLException {
        URL url = new URL("http");
        String currency = null;
        urlParser.parse(url, currency);
    }

    @Test
    public void testValidURLAndNullCurrency() throws MalformedURLException {
        URL url = new URL("http://www.nbp.pl/kursy/xml/c073z070413.xml");
        String currency = null;
        Assert.assertNull(urlParser.parse(url, currency));
    }

    @Test
    public void testValidURLAndInvalidCurrency() throws MalformedURLException {
        URL url = new URL("http://www.nbp.pl/kursy/xml/c073z070413.xml");
        String currency = "ddd";
        Assert.assertNull(urlParser.parse(url, currency));
    }

    @Test
    public void testValidURLAndValidCurrency() throws MalformedURLException {
        URL url = new URL("http://www.nbp.pl/kursy/xml/c073z070413.xml");
        String currency = "EUR";
        List<Position> positions = urlParser.parse(url, currency);
        Assert.assertNotNull(positions);
        Assert.assertEquals(1, positions.size());
    }

}
